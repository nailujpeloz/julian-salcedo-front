import { Log } from './log';

export class Matrix {
    //Model of 3D Matrix
    private _structure : number[][][];
    //Number of rows on each dimension 
    private _N : number;
    // Logs to register changes 
    private _log : Log;

    constructor(N : number, log : Log){
        this._N = N;
        this._structure = [];
        this._log = log;
        for(var x : number = 0; x < this._N ; x++){
            this._structure[x] = [];
            for(var y : number = 0; y < this._N ; y++){
                this._structure[x][y] = [];
                for(var z : number = 0; z < this._N ; z++){
                    this._structure[x][y][z] = 0;
                }
            }
        }
    }
    // Getter Matrix size
    // return _N ; Return actual size of matrix on each dimension
    get N () {
        return this._N;
    }
    // Setter Matrix size
    // @N : number ; Size of matrix on each dimension
    set N (N : number){
        this._N = N;
    }
    // Getter Matrix
    // return _structure ; Return actual matrix structure
    get structure(){
        return this._structure;
    }
    // Setter Matrix
    // @structure : number [][][] ; New structure
    set structure(structure : number[][][]){
        this._structure = structure;
    }
    // Getter Log of operations
    // return _log.mainLog ; Return actual operation's log
    get operationsLog(){
        return this._log.mainLog;
    }
    // Getter Log of output
    // return _log.minLog ; Return actual output's log
    get outputLog(){
        return this._log.minLog;
    }
    // Function which update an specific value of matrix
    // @x : number ; X Coordinate
    // @y : number ; Y Coordinate
    // @z : number ; Z Coordinate
    // @w : number ; new value
    updateValue(x : number, y : number, z : number, w : number){
        try{
            this._structure [x][y][z] = w;
            this._log.addFullLog("UPDATE "+x+" "+y+" "+z+" "+" "+w);
        }catch(e){
            alert("Out of index. Try using different values for x or y or z. Please check that all of them are lower than: "+this._N);
        }
    }
    // Function which calculate sum of specific values on matrix
    // @x1 : number ; First X Coordinate
    // @y1 : number ; First Y Coordinate
    // @z1 : number ; First Z Coordinate
    // @x2 : number ; Second X Coordinate
    // @y2 : number ; Second Y Coordinate
    // @z2 : number ; Second Z Coordinate
    queryValue(x1 : number, y1 : number, z1 : number, x2 : number, y2 : number, z2 : number){
        try{
            var sum : number = 0;
            for(var x : number = x1; x < x2; x++){
                for(var y : number = y1; y < y2; y++){
                    for(var z : number = z1; z < z2; z++){
                        sum += this._structure[x][y][z];   
                    }
                }
            }
            this._log.addFullLog("QUERY "+x1+" "+y1+" "+z1+" "+x2+" "+y2+" "+z2);
            this._log.addMinLog(sum);
            return sum
        }catch(e){
            alert("Out of index. Try using different values for x or y or z. Please check that all of them are lower than: "+this._N)
        }
    }
}
