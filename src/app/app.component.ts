import { Log } from './log';
import { create } from 'domain';
import { NgForm } from '@angular/forms/src/directives';
import { FormBuilder, FormGroup, Validator, Validators, FormArray } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Matrix } from './matrix';
import { Validators as cValidator} from './validators'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  // Main form
  matrixForm : FormGroup;
  // Flag to show submit button
  private showSubmit = true;
  // Custom validators
  private testCases : any [] = [];
  // Number of test cases
  private t : number = 0;
  //General log instannce
  private log : Log ;

  constructor (private validator : cValidator){
    this.log = new Log();
    validator = new cValidator();
  }

  ngOnInit(){}

  initTestCases () {
    var test = this.validator.validateT(this.t);
    if(!test){
      console.log("T is not valid. Must between 1 - 50. Or is not a number");
    }else{
      for(var i : number = 0; i < this.t ; i++){
        this.testCases.push(this.generateTestCaseObject());
      }
    }
  }

  initSpecificTestCase(index : number) {
    var test = false;
    if(test){
      console.log("Validacion especifica");
    }else{
      for(var i : number = 0; i < this.testCases[index].m ; i++){
        this.testCases[index].operations.push(this.generateOperationObject());
      }
    }
  }

  calculateExercise(){
    var test = false;
    if(test){
      console.log("Validacion Total");
    }else{
      for(var tIndex : number = 0; tIndex < this.testCases.length ; tIndex ++){
        var tempMatrix = new Matrix(this.testCases[tIndex].n,this.log);
        for(var tOperation : number = 0; tOperation < this.testCases[tIndex].m ; tOperation ++ ){
          console.log("operations", this.testCases[tIndex].m);
          console.log("array ", this.testCases[tIndex]['operations']);
          if(this.testCases[tIndex]['operations'][tOperation].type == 'q'){
            console.log("Query");
            var values = this.extractArrayValues(this.testCases[tIndex]['operations'][tOperation].value);
            tempMatrix.queryValue(parseInt(values[0]),parseInt(values[1]),parseInt(values[2]),
              parseInt(values[3]),parseInt(values[4]),parseInt(values[5]));
          }
          if(this.testCases[tIndex]['operations'][tOperation].type == 'u'){
            console.log("Update");
            var values = this.extractArrayValues(this.testCases[tIndex]['operations'][tOperation].value);
            tempMatrix.updateValue(parseInt(values[0]),parseInt(values[1]),
            parseInt(values[2]),parseInt(values[3]));
          }
          
          
        }
      }
    }
  }

  generateTestCaseObject (){
    return  {
      'n' : 0,
      'm' : 0,
      'operations' : [] 
    }
  }

  generateOperationObject(){
    return {
      'type' : '',
      'value' : ''
    }
  }

  extractArrayValues (data : string) : string[]{
    var result : string [] = null;
    result = data.split(',');
    return result
  }

}