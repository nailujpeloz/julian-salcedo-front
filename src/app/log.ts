export class Log {
    //Main log register
    private _mainLog : string [];
    //Minified log register
    private _minLog : string [];

    constructor(){
        this._mainLog = [];
        this._minLog = [];
    }

    //Getter of actual full log
    //return _mainLog ; actual mainLog state
    get mainLog(){
        return this._mainLog;
    }
    //Setter full log
    //@mainLog : string [] ; new main log
    set mainLog(mainLog : string[]){
        this._mainLog = mainLog;
    } 
    //Getter of actual full minified log
    //return _minLog ; actual minLog state
    get minLog(){
        return this._minLog;
    }
    //Setter full minified log
    //@minLog : string [] ; new min log
    set minLog(minLog : string[]){
        this._minLog = minLog;
    } 
    //Add register to full log
    addFullLog(data){
        this._mainLog.push(data);
    }
    //Add register to min log
    addMinLog(data){
        this._minLog.push(data);
    }

    
}
