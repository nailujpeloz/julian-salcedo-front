import { JulianSalcedoFrontPage } from './app.po';

describe('julian-salcedo-front App', function() {
  let page: JulianSalcedoFrontPage;

  beforeEach(() => {
    page = new JulianSalcedoFrontPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
